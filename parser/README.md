#Test :
----------------------------------------------------
We need a Java application that will consume a CSV file,
parse the data and insert to a SQLite In-Memory database.

a. Table X has 10 columns A, B, C, D, E, F, G, H, I, J which correspond with the CSV file column header
names.

b. Include all DDL in submitted repository. Note : DDL is created using Java code. while system comes its creates automatically.

c. Create your own SQLite DB

#Development and Compiling Requirements :
----------------------------------------------------

####Operating System:

Used windows 10.

####Java:
 We've used Latest Java-version downloaded from
 
 [java](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)

#### Sqlite:
1. Download latest sqlite3 and install from 
[sqlite](https://www.sqlite.org/index.html)

2. Extract the Sqlite and set path varible for sqlite.

### Maven and Eclipse :

1. Install latest version of Maven and setup environment for Ecclipse.

# Maven dependencies and Application structure.

####Maven dependencies.

1. sqlite-jdbc-3.7.2 version for Sqlite-Jdbc driver to connect to the database.
2. commons-lang3-3.0 for String utils for parsing the string.
3. Configured maven-compiler-plugin to set Java compilation version to java-8.
4. configured maven-jar-plugin to create executable jar file.

```xml
<mainClass>
com.parser.database.Main # Looks for main method to start main thread.
</mainClass>
```

####Application Directory structure.
parser is the application directory. The below directory consists in parser/ parent directory
1. src : source is source folder for all source code.

2. target : target folder consists of all compiled class ,test-classes, compiled-executable jar files, application.properties and exe.bat.

------> compiled executable jar file, exe.bat and application.properties should consists in same folder.

------> To change configuration, change the application.properties and execute exe.bat file. The keys in the property files should remain same and values can be changed.

3. application.properties : the same application.properties which is used in target/ folder is used for testing , to remain code unchanged.

4. pom: pom (Project Object Model) xml file used to download and build the application.

# Code walk through:

####Package : com.parser.database Contains below Classes:

##1. Main.java : methods
	
```java

private static void execute() # this method executes for every 1 minute by the scheduler. This scheduler checks any new CSV files available in the path. If available, parses and update in DB.


private static void prettyPrint() # this method prints the record statistics for every 15 seconds. This made to print every 15 seconds, because if any large file with too many records. The thread executes in background which may not reflect immediately.

public static void main(String[] args) # main method consists of Banner and Executor threads to schedule the execute() method and prettyprint() for statistics. 
```
##2. CsvUtils.java
```java
public static void parseCsv() # Method uses to check if any files available in the configured path. This invokes the parseFile() to parse individual file located in the configured path.

private static void parseFile(File f) # Method reads the csv file line by line and splits the comma seperated into String[] and added to Executors.scheduled thread pool which internally maintain queue. In background it updates Store.

private static void updateRecords(String[] csvRecord, String filename) # method used to fetch the record from scheduler and update to database. Failed records update to failed path (<filename_Error.csv).

public static void writeErrorFiles(String[] csvRecord, String filename) # method wirteErrorFiles to Error files.
```

##3. SqliteDatabase.java
```java

static {
		crateParserTable(); # to create parser table, CSV data which needs to update in this table.
		createStats(); # statistics table
	}  ## static block code to create DDL structure in DB on application startup if doesn't exists.
	

public static Connection connect()  # utility method to get connection object.

public static void insertStatsTable() # Method to insert data into Stats table.

public static List<Stats> getStats() # utility method to getStatus information. Query : 'Select file,sum(received),sum(success), sum(failed) from stats group by file'

insertParserTable() # method to insert data into parser table.

static class Stats # Pojo class to fetch stats information.
```

##4. CSVReader.java

```java
CSVReader.java consume the Reader object and parses line by line  and columns using comma seperated.
```

##5. PrettyTable.java

```java
PrettyTable.java is the utility class to print data in tabular form beautifully.
```

##6. ImageUtils.java

```java
ImageUtils.java is utility class to download the png images using the byte string comming from CSV file.
```
##7. Constants.java

```java
Constants.java is used to keep the constants in single files parsed from application.properties. The idea is to reuse across the applications.
```
# Useful commands.

1. mvn clean install to download the dependencies and build the package.
2. To verify the data in database go to Database path (configured in application.properties file) and run CMD tool in windows.
3. run the command ( sqlite3 <databasename>)
4. run the command (.tables) in the terminal to verify the databases.
3. run select queries to verify the commands.

# How to run executable ?

1. To the application successfully, the jar file, application.properties and exe.bat file should be in same folder.

2. we can copy these 3 files in same folder even outside of the target/ folder.

NOTE : The code is platform independent but the path configured in application.properties file is OS dependent


# THANK YOU :)



