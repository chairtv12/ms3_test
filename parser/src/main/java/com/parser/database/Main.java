package com.parser.database;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {
	public static void main(String[] args) {
		String banner = "\n" + "Welcome to test :) \n" + " __  __  _____ ____  \r\n" + " |  \\/  |/ ____|___ \\ \r\n"
				+ " | \\  / | (___   __) |\r\n" + " | |\\/| |\\___ \\ |__ < \r\n" + " | |  | |____) |___) |\r\n"
				+ " |_|  |_|_____/|____/ \r\n" + "                      ";

		System.out.println(banner);
		try {
			Constants.createRequiredDirectories();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		  ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();
		  es.scheduleAtFixedRate(() -> execute(), 0, 1, TimeUnit.MINUTES);
		  es.scheduleAtFixedRate(() -> prettyPrint(), 1, 15, TimeUnit.SECONDS);
		 
	}

	private static void execute() {
		System.out.println("Scanning files");
		CompletableFuture<Void> future2 = CompletableFuture.runAsync(() -> CsvUtils.parseCsv());
		future2.thenRun(() -> prettyPrint());
	}

	private static void prettyPrint() {
		PrettyTable pt = new PrettyTable("File Name", "#Records Received", "#Records Success", "#Records Failed");
		SqliteDatabase.getStats().forEach(obj -> {
			pt.addRow(obj.file(), obj.recived(), obj.success(), obj.failed());
		});
		System.out.println(pt.toString());
	}
}
