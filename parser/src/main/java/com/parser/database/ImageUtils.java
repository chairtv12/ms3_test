package com.parser.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;

public class ImageUtils {

	public void generateImageFile(String byteString, String filename) {
		OutputStream os = null;
		byte[] getBytes = Base64.getDecoder().decode(byteString);

		try {
			File newFile = new File(Constants.IMAGE_PATH + filename);
			os = new FileOutputStream(newFile);
			os.write(getBytes);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
}
