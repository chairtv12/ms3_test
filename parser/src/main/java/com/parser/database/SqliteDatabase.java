package com.parser.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

public class SqliteDatabase {

	private static final String DB_DRIVER = "jdbc:sqlite:";

	// Loading the sqlite jdbc driver.
	static {
		crateParserTable();
		createStats();
	}

	// getting database connection object.
	public static Connection connect() {
		Connection con = null;
		String url = DB_DRIVER + Constants.DATA_BASE_PATH + Constants.DATABASE_NAME;
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection(url);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return con;
	}

	public static void crateParserTable() {
		// SQL statement for creating a new table
		String sql = "CREATE TABLE IF NOT EXISTS parse (\n" + "	fname text ,\n" + "	lname text ,\n" + "	email text ,\n"
				+ "	gender text ,\n" + "	image blob ,\n" + "	cardtype text ,\n" + "	currency text ,\n"
				+ "	flag1 text ,\n" + "	flag2 text ,\n" + "	city text ,\n" + "PRIMARY KEY (fname, lname, email)" + ");";

		try (Connection conn = connect(); Statement stmt = conn.createStatement()) {
			stmt.execute(sql);
		} catch (SQLException e) {
		}
	}

	public static void createStats() {
		// SQL statement for creating a new table
		String sql = "CREATE TABLE IF NOT EXISTS stats (\n" + "	file text ,\n" + "	received int ,\n"
				+ "	success int ,\n" + "	failed text \n );";

		try (Connection conn = connect(); Statement stmt = conn.createStatement()) {
			stmt.execute(sql);
		} catch (SQLException e) {
		}
	}

	public static void insertStatsTable(String file, int received, int success, int failed) {
		String query = "insert into stats (file,received,success,failed) values (?,?,?,?)";

		try {
			Connection conn = connect();
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setString(1, file);
			pstmt.setInt(2, received);
			pstmt.setInt(3, success);
			pstmt.setInt(4, failed);
			pstmt.execute();
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Stats> getStats() {
		String query = "Select file,sum(received),sum(success), sum(failed) from stats group by file";
		Connection con = null;
		Statement ps = null;
		ResultSet rs = null;
		List<Stats> stats = new ArrayList<SqliteDatabase.Stats>();
		try {
			con = connect();
			ps = con.createStatement();
			rs = ps.executeQuery(query);
			while (rs.next()) {
				stats.add(new SqliteDatabase.Stats(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				ps.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return stats;
	}

	public static void insertParserTable(String[] csvRecord, String filename) {
		List<String> list = Arrays.asList(csvRecord);
		// Map<String, String> map = csvRecord.toMap();
		String updateQuery = "insert into parse (fname,lname,email,gender,image,cardtype,currency,flag1,flag2,city) values (?,?,?,?,?,?,?,?,?,?) ";
		try {
			Connection conn = connect();
			PreparedStatement pstmt = conn.prepareStatement(updateQuery);
			String blobStr = list.get(4);
			byte[] b = null;
			if (Objects.nonNull(blobStr)) {
				blobStr = blobStr.split(",")[1];
				b = Base64.getDecoder().decode(blobStr);
			}
			pstmt.setString(1, list.get(0));
			pstmt.setString(2, list.get(1));
			pstmt.setString(3, list.get(2));
			pstmt.setString(4, list.get(3));
			pstmt.setBytes(5, b);
			pstmt.setString(6, list.get(5));
			pstmt.setString(7, list.get(6));
			pstmt.setString(8, list.get(7));
			pstmt.setString(9, list.get(8));
			pstmt.setString(10, list.get(9));
			int i = pstmt.executeUpdate();
			insertStatsTable(filename, 1, i, 0);
			//System.out.print(".");
			pstmt.close();
			conn.close();
		} catch (SQLException e) {
			CsvUtils.writeErrorFiles(csvRecord, filename);
			insertStatsTable(filename, 1, 0, 1);
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	static class Stats {
		private String file;
		private String recived;
		private String success;
		private String failed;

		public Stats(String file, String recived, String success, String failed) {
			super();
			this.file = file;
			this.recived = recived;
			this.success = success;
			this.failed = failed;
		}

		public String file() {
			return file;
		}

		public String recived() {
			return recived;
		}

		public String success() {
			return success;
		}

		public String failed() {
			return failed;
		}

	}

}
