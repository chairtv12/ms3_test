package com.parser.database;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;

public class CsvUtils {
	public static final int COLUMN_COUNT = 10;
	public static final String ERROR_FILE = "_Error";
	public static ScheduledExecutorService service = Executors.newScheduledThreadPool(1);

	public static void parseCsv() {
		File[] files = new File(Constants.CSV_PATH).listFiles();
		if (files.length == 0) {
			System.out.println(String.format("Sorry not able to find files in location %s", Constants.CSV_PATH));
			return;
		}
		for (File file : files) {
			parseFile(file);
		}
	}

	private static void parseFile(File f) {
		FileReader fr = null;
		CSVReader reader = null;
		System.out.println(String.format("Started %s file processing", f.getName()));
		try {
			//synchronized (f) {
				fr = new FileReader(f);
				reader = new CSVReader(fr);
				while (reader.hasNext()) {
					String[] line = reader.readNext();
					service.submit(() -> updateRecords(line, f.getName()));
				}
			//}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fr.close();
				System.out.println(String.format("%s file processing completed, deleting the file."
						+ "\n updating the records background."
						+ "\n Takes time to completly update the records.", f.getName()));
				Files.deleteIfExists(Paths.get(f.getPath()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void updateRecords(String[] csvRecord, String filename) {
		if (csvRecord.length != COLUMN_COUNT) {
			writeErrorFiles(csvRecord, filename);
			SqliteDatabase.insertStatsTable(filename, 1, 0, 1);
			return;
		}
		SqliteDatabase.insertParserTable(csvRecord, filename);
	}

	public static void writeErrorFiles(String[] csvRecord, String filename) {
		File f = new File(Constants.FAILED_RECORDS_PATH + ERROR_FILE + filename);
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String s = Arrays.asList(csvRecord).stream().collect(Collectors.joining(","));
		try (FileWriter fw = new FileWriter(f)) {
			fw.write(s);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
