package com.parser.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public final class Constants {
	public static String DATABASE_NAME;
	public static String DATA_BASE_PATH;
	public static String IMAGE_PATH;
	public static String CSV_PATH;
	public static String FAILED_RECORDS_PATH;
	

	// creating required directories and generating the DB urls.
	// Setting all constants.
	public static void createRequiredDirectories() throws FileNotFoundException, IOException {
		Properties ps = new Properties();
		
		System.out.println(new File("application.properties").getAbsolutePath());
		ps.load(new FileReader(new File("application.properties")));
		String optionalPath = new File(new File(".").getAbsolutePath()).getPath();
		for (Object s : ps.keySet()) {
			String key = (String) s;
			if (!key.equals("SQL_DATABASE_NAME")) {
				File dir = new File(ps.getProperty(key));
				if (!dir.exists() || !dir.isDirectory()) {
					dir.mkdir();
					System.out.println(String.format("Directory %s created", dir.getName()));
				} else {
					System.out.println(String.format("Directory %s already exists", dir.getName()));
				}
				String val = (String) ps.getProperty(key);
				String value = Objects.isNull(val) || val.isEmpty() ? optionalPath : val;
				switch (key) {

				case "IMAGE_PATH":
					IMAGE_PATH = value;
					break;
				case "CSV_PATH":
					CSV_PATH = value;
					break;
				case "DATA_BASE_PATH":
					DATA_BASE_PATH = value;
					break;
				case "FAILED_RECORDS_PATH":
					FAILED_RECORDS_PATH = value;
					break;
				default:
					System.out.println("Invalid key!!");
					break;
				}
			}

		}
		String dbName = (String) ps.get("SQL_DATABASE_NAME");
		DATABASE_NAME = Objects.isNull(dbName) || dbName.isEmpty() ? "parser.db" : dbName;
	}
}
